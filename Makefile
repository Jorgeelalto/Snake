CC = gcc
CFLAGS = -O3 -g -Wall -pedantic

all:
	$(CC) $(CFLAGS) -o snake snake.c
