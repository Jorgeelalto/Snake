#include "stdio.h"
#include "unistd.h"
#include <time.h>
#include <stdlib.h>

#define ORIENTATION_INERTIA 10
#define DIMENSION 24
#define FRAME_DELAY 100
#define FOOD_SCARCITY 15

//Euclidean modulo function
int mod(int a, int base) {
    return (a < 0 ? ((a % base) + base) % base : a % base);
}

int main() {
	
	//Start random generator
	srand(time(NULL));
	//Board space
	int s[DIMENSION][DIMENSION];
    //Some auxiliary stuff
	int i,j;
    //Generate the tiles
	for (i = 0; i < DIMENSION; i++) {
			for (j = 0; j < DIMENSION; j++) {
            //Almost all of them are empty,
            //but some of them is food
            if (((int) (rand() % FOOD_SCARCITY)) == 0) s[i][j] = -1;
            else s[i][j] = 0;
		}
	}

	//X position of the snake head
	int x = 8;
	//Y position of the snake head
	int y = 8;
	//Head orientation:
	//0=right; 1=down, 2=left, 3=up
	int d = 0;
	//Length of the snake
	int l = 5;
    //Maximum number of steps
	int aux = 0;
    //Stuckness
    int stuck = 0;

	while (aux++ < 1024) {

		//Set a body here
		s[x][y] = l;
		//Change orientation randomly
		if (rand() % ORIENTATION_INERTIA == 0) {
			//While the new orientation is the same we
			//had previously, choose a new one depending
			//on the current orientation:
		    if (rand() % 2) d = mod((d + 1), 4);
            else            d = mod((d - 1), 4);
		}

        //If there is body ahead, force the change of orientation
        //First check if the snake is stuck
    
        if (s[x+1][y] > 0) stuck++;
        if (s[x][y-1] > 0) stuck++;
        if (s[x-1][y] > 0) stuck++;
        if (s[x][y+1] > 0) stuck++;
        //If there are three sides unaccessible then the snake is stuck
        if (stuck == 4) {
            printf("The snake got stuck with itself!\n");
            exit(0);
        } else stuck = 0;
            
        while ((d == 0 && s[x+1][y] > 0) || (d == 1 && s[x][y-1] > 0) ||
            (d == 2 && s[x-1][y] > 0) || (d == 3 && s[x][y+1] > 0)) {

		    if (rand() % 2) d = mod((d + 1), 4);
            else            d = mod((d - 1), 4);
        }

        //Detect nearby food
        if ((x > 1 && x < DIMENSION - 1) && (y > 1 && y < DIMENSION - 1)) {
            //Right
            if      ((s[x+1][y] < 0) || (s[x+2][y] < 0)) d = 0;
            //Down
            else if ((s[x][y-1] < 0) || (s[x][y-2] < 0)) d = 1;
            //Left
            else if ((s[x-1][y] < 0) || (s[x-2][y] < 0)) d = 2;
            //Up 
            else if ((s[x][y+1] < 0) || (s[x][y+2] < 0)) d = 3;
        }

		//Move the head
        if        (d == 0) {
            //Right
            if (x == DIMENSION - 1) x = 0;
            else x++;
		} else if (d == 1) {
            //Down
            if (y == 0) y = DIMENSION - 1;
            else y--;
        } else if (d == 2) {
            //Left
            if (x == 0) x = DIMENSION - 1;
            else x--;
        } else if (d == 3) {
            //Up
            if (y == DIMENSION - 1) y = 0;
            else y++;
        }

        //Check if the snake is over a food tile, if it is
        //then eat it and become longer 
        if (s[x][y] < 0) l++;
		//Print some info
		printf("\n");
        printf("Orientation: %i\n", d);
        printf("Tail length: %i\n", l);
        printf("Step: %i\n", aux);
		//Print the board
		//Line by line
		for (i = 0; i < DIMENSION + 2; i++) printf("--");
		printf("\n");
		for (i = 0; i < DIMENSION; i++) {
			//Print the board padding in this line
			printf("| ");
			//Char per char
			for (j = 0; j < DIMENSION; j++) {
				//If the position matches the head
				//coordinates; then print it
				if (i == x && j == y) printf("@ ");
				//If there is a zero then print empty space
				else if (s[i][j] == 0) printf("  ");
				//If there is a number bigger than zero
				//there is a snake body part; print a #
				//and change the alive time left
				else if (s[i][j] > 0) {
					printf("# ");
					s[i][j]--;
				}
				//if the number is smaller than zero
				//there is food
				else printf("* ");
			}
			printf(" |\n");
		}
        //Print the bottom of the board
		for (i = 0; i < DIMENSION + 2; i++) printf("--");
		printf("\n");
        //Wait some time so the FPS are not too high
		usleep((FRAME_DELAY * 1000));
		printf("\n\n");
	}
	return 0;
}
