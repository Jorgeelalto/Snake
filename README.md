# Snake
Snake in C dsadsa

# Configuration
The program is configured via the constants at the beginning of the source
code:
* ORIENTATION_INERTIA: How hard is for the snake to turn. Higher values make
    it turn less often. A value of around 6-8 is an ideal rate.
* DIMENSION: The size of the map, in tiles per axis. Take into account that
    making the map bigger does automatically increase the food supply.
* FRAME_DELAY: The delay between each frame or step of the program. A value
    between 100-200 is enough to see clearly the details.
* FOOD_SCARCITY: How hard is for the map generator to create food at the
    beginning of the program. Higher values create less food in the map. A
    value between 15-20 creates a neat distribution.
